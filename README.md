Contents
========
* [Introduction](#introduction)
* [Adding to your project](#adding-to-your-project)
  * [Standalone](#standalone)
  * [Gyro](#gyro)
  * [Zigmod](#zigmod)
* [Usage](#usage)
  * [Creating a *color* struct](#creating-a-color-struct)
  * [Converting storage types](#converting-storage-types)
  * [Converting color spaces](#converting-color-spaces)
  * [Converting to and from hex](#converting-to-and-from-hex)
* [Todo](#todo)
## Introduction
**Zig-color** is a small standalone library written in **Zig** for the storage
and manipulation of color values in RGBA, CMYK, and HSL color spaces using any
width unsigned integer or float type for storage. When complete, conversions
between different color spaces as well as input via `[]const u8` Zig strings and
slices and hexadecimal `[]const u8` strings and slices will be supported.
## Adding to your project
### Standalone
Just create a git submodule
### Gyro
### Zigmod
In your `zig.mod` file, add the following:
```Yaml
dev_dependencies:
  - src: git https://codeberg.org/jeang3nie/zig-color
    name: color
    main: src/lib.zig
```
Add to your `zig.mod` file
```Yaml
dependencies:
  - src: git https://codeberg.org/jeang3nie/zig-color
```
## Usage
### Creating *color* structs
```Zig
// create `red` as an RGBA color struct with u8 storage
const redint = try RGBA.new(u8, 255, 0, 0, 255);
try testing.expect(redint.red == 255);
try testing.expect(redint.green == redint.blue);
try testing.expect(redint.alpha == 255);
// The same, but using f64 storage
const redfloat = try RGBA.new(f64, 1.0, 0.0, 0.0, 1.0);
// Creating an HSL (Hue, Saturation, Lightness) struct
const hsl_color = try HSL.new(f64, 0.345, 0.865, 0.465);
// Creating a CMYK (Cyan, Magenta, Yellow, Black) struct
const cmyk_color = try CMYK.new(f32, 1.0, 0.0, 0.5, 0.21);
```
> Note: for integer storage in RGBA color space, the values range from 0 to
> math.maxInt(type), while for float types the values are bounded from 0.0 to
> 1.0. Think of the values as a percentage of the maximum.
>
> For CMYK color space, float values range from 0 to 1 as above, while integer
> values range between 0 and 100, as per convention.
>
> For HSL color space, `hue` ranges from 0 to 360 regardless of storage type.
> The `saturation` and `luminence` fields range from 0 to 100 for integer
> types and 0 to 1 for float types.

### Converting storage types
```Zig
// convert an RGBA color using u8 storage to f64 storage
const green = try RGBA.new(u8, 0, 255, 0, 255);
const new_green = try green.toFloat(f64);
```
### Converting color spaces
```Zig
// convert RGBA to CMYK
const myAwesomeColor = try RGBA.new(u8, 127, 0, 200, 255);
const awesome_cmyk = try myAwesomeColor.toCMYK(u8);
try testing.expect(awesome_cmyk.cyan == 37);
### Converting to and from hex
```Zig
// Parse a `white` hexadecimal string into an RGBA struct
const white = try RGBA.fromHex(u8, "#ffffff");
// And put it back again
const white_hex = try white.toHex(std.testing.allocator);
// The same works with CMYK
const red = try CMYK.fromHex(u8, "#ff0000")
```
### Todo
* RGBA
  * from []const u8
* CMYK
  * from []const u8
* HSL
  * from []const u8
