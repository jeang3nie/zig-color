const std = @import("std");
const fmt = std.fmt;
const math = std.math;
const mem = std.mem;
const testing = std.testing;
const com = @import("common.zig");
const CMYK = @import("cmyk.zig");
const RGBA = @import("rgba.zig").RGBA;

pub const HSL = struct {
    hue: anytype,
    saturation: anytype,
    luminence: anytype,

    const Self = @This();

    pub fn new(comptime T: type, hue: anytype, saturation: anytype, luminence: anytype) com.ColorTypeError!Self{
        const typeinfo = @typeInfo(T);
        switch (typeinfo) {
            .Int => |info| {
                if (math.maxInt(T) < 360) return error.UnsupportedType;
                if (hue > 360 or saturation > 100 or luminence > 100)
                    return error.OutOfBounds;
                return switch (info.signedness) {
                    .unsigned => Self{
                        .hue = hue,
                        .saturation = saturation,
                        .luminence = luminence,
                    },
                    else => error.UnsupportedType,
                };
            },
            .Float => {
                return if (com.checkBounds(saturation)
                    and com.checkBounds(luminence)) Self{
                    .hue = hue,
                    .saturation = saturation,
                    .luminence = luminence,
                } else error.OutOfBounds;
            },
            else => return error.UnsupportedType,
        }
    }

    pub fn toInt(self: anytype, comptime T: type) !Self {
        const oldtype = @TypeOf(self.hue);
        const typeinfo = @typeInfo(T);
        // Types are identical, regurgitate everything as is
        if (oldtype == T) return Self{
            .hue = self.hue,
            .saturation = self.saturation,
            .liminence = self.luminence,
        };
        switch (typeinfo) {
            .Int => |info| {
                return switch (info.signedness) {
                    .unsigned => Self{
                        .hue = @intCast(T, self.hue),
                        .saturation = @intCast(T, self.saturation),
                        .luminence = @intCast(T, self.luminence),
                    },
                    else => error.UnsupportedType,
                };
            },
            else => return error.UnsupportedType,
        }
    }

    pub fn toFloat(self: anytype, comptime T: type) !Self {
        const oldtype = @TypeOf(self.hue);
        const typeinfo = @typeInfo(T);
        if (oldtype == T) return HSL{
            .hue = self.hue,
            .saturation = self.saturation,
            .luminence = self.luminence,
        };
        return switch (typeinfo) {
            .Float => HSL{
                .hue = @floatCast(T, self.hue),
                .saturation = @floatCast(T, self.saturation) / 100.0,
                .luminence = @floatCast(T, self.luminence) / 100.0,
            },
            else => error.UnsupportedType,
        };
    }


    pub fn toRGBA(self: anytype, comptime T: type) com.ColorTypeError!RGBA {
        const fl = try self.toFloat(f64);
        const chroma = (1 - math.absFloat(2 * fl.luminence - 1)) * fl.saturation;
        const x = chroma * (1 - math.absFloat((fl.hue / 60) % 2 - 1));
        // We add this amount to each channel to account for luminence
        const match = fl.luminence - (chroma / 2);
        var red: f64 = match;
        var green: f64 = match;
        var blue: f64 = match;
        if (fl.hue >= 0 and fl.hue < 60 or fl.hue == 360) {
            red += chroma;
            green += x;
        } else if (fl.hue >= 60 and fl.hue < 120) {
            red += x;
            green += chroma;
        } else if (fl.hue >= 120 and fl.hue < 180) {
            green += chroma;
            blue += x;
        } else if (fl.hue >= 180 and fl.hue < 240) {
            green += x;
            blue += chroma;
        } else if (fl.hue >= 240 and fl.hue < 300) {
            red += x;
            blue += chroma;
        } else if (fl.hue >= 300 and fl.hue < 360) {
            red += chroma;
            blue += x;
        } else return error.OutOfBounds;
        switch (@typeInfo(T)) {
            .Float => return RGBA.new(
                T,
                @floatCast(T, red),
                @floatCast(T, green),
                @floatCast(T, blue),
                1.0),
            .Int => |info| {
                return switch (info.signedness) {
                    .unsigned => try RGBA.new(
                        T,
                        @floatToInt(T, math.round(red * @intToFloat(f64, math.maxInt(T)))),
                        @floatToInt(T, math.round(green * @intToFloat(f64, math.maxInt(T)))),
                        @floatToInt(T, math.round(blue * @intToFloat(f64, math.maxInt(T)))),
                        math.maxInt(T)
                    ),
                    else => error.UnsupportedType,
                };
            },
            else => return error.UnsupportedType,
        }
    }

    pub fn toCMYK(self: anytype, comptime T: type) com.ColorTypeError!CMYK {
        const color = try self.toRGBA(f64);
        return try color.toCMYK(T);
    }

    pub fn toHex(self: anytype, allocator: mem.Allocator) ![]const u8 {
        const color = try self.toRGBA(u8);
        return try color.toHex(allocator);
    }

    pub fn toHexZ(self: anytype, allocator: mem.Allocator) ![]const u8 {
        const color = try self.toRGBA(u8);
        return try color.toHexZ(allocator);
    }

    pub fn fromHex(comptime T: type, hexcolor: []const u8) !HSL {
        const rgba = try RGBA.fromHex(u64, hexcolor);
        return try rgba.toHSL(T);
    }

    pub fn toString(self: anytype, allocator: mem.Allocator) ![]u8 {
        return try fmt.allocPrint(allocator, "HSL({d}, {d}, {d})",
            .{self.hue, self.saturation, self.luminence});
    }

    pub fn toStringZ(self: anytype, allocator: mem.Allocator) ![]u8 {
        return try fmt.allocPrintZ(allocator, "HSL({d}, {d}, {d})",
            .{self.hue, self.saturation, self.luminence});
    }
};

test "new: f32" {
    const color = try HSL.new(f32, 0.5, 0.64, 0.6);
    try testing.expect(color.hue == 0.5);
    try testing.expect(color.saturation == 0.64);
    try testing.expect(color.luminence == 0.6);
}

test "new: f64" {
    const color = try HSL.new(f64, 0.5, 0.64, 0.6);
    try testing.expect(color.hue == 0.5);
    try testing.expect(color.saturation == 0.64);
    try testing.expect(color.luminence == 0.6);
}

test "new: unsupportedType: u8" {
    _ = HSL.new(u8, 0, 0, 0) catch |err| switch (err) {
        error.UnsupportedType => return {},
        else => |e| return e,
    };
    try testing.expect(false);
}

test "new: unsupportedType: Int" {
    _ = HSL.new(i8, 0, 0, 0) catch |err| switch (err) {
        error.UnsupportedType => return {},
        else => |e| return e,
    };
    try testing.expect(false);
}

test "new: unsupportedType: bool" {
    _ = HSL.new(bool, true, false, true) catch |err| switch (err) {
        error.UnsupportedType => return {},
        else => |e| return e,
    };
    try testing.expect(false);
}

test "new: OutOfBounds: negative" {
    _ = HSL.new(f32, 0.1, -0.1, 0.0) catch |err| switch (err) {
        error.OutOfBounds => return {},
        else => |e| return e,
    };
    try testing.expect(false);
}

test "new: OutOfBounds: high" {
    _ = HSL.new(f32, 0.1, 0.0, 2.0) catch |err| switch (err) {
        error.OutOfBounds => return {},
        else => |e| return e,
    };
    try testing.expect(false);
}

test "fromHex" {
    const hex = "#fe246e";
    const hsl = try HSL.fromHex(u32, hex);
    try testing.expect(hsl.hue == 340);
    try testing.expect(hsl.saturation == 99);
    try testing.expect(hsl.luminence == 57);
}

test "toRGBA" {
    const c = try HSL.new(u32, 127, 45, 55);
    const color = try c.toRGBA(u8);
    try testing.expect(color.red == 89);
    try testing.expect(color.green == 192);
    try testing.expect(color.blue == 101);
}

test "toString" {
    const color = try HSL.new(u16, 23, 45, 85);
    const color_str = try color.toString(testing.allocator);
    defer testing.allocator.free(color_str);
    try testing.expect(mem.eql(u8, color_str, "HSL(23, 45, 85)"));
}

test {
    @import("std").testing.refAllDecls(@This());
}
