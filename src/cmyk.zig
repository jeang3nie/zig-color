const std = @import("std");
const fmt = std.fmt;
const math = std.math;
const mem = std.mem;
const testing = std.testing;
const com = @import("common.zig");
const HSL = @import("hsl.zig").HSL;
const RGBA = @import("rgba.zig").RGBA;

pub const CMYK = struct {
    cyan: anytype,
    magenta: anytype,
    yellow: anytype,
    black: anytype,

    const Self = @This();

    pub fn new(comptime T: type, c: T, m: T, y: T, k: T) com.ColorTypeError!Self {
        switch (@typeInfo(T)) {
            .Int => |info| {
                return switch (info.signedness) {
                    .unsigned => Self{
                        .cyan = c,
                        .magenta = m,
                        .yellow = y,
                        .black = k,
                    },
                    else => error.UnsupportedType,
                };
            },
            .Float => {
                return if (com.checkBounds(c)
                    and com.checkBounds(m)
                    and com.checkBounds(y)
                    and com.checkBounds(k)) Self{
                    .cyan = c,
                    .magenta = m,
                    .yellow = y,
                    .black = k,
                } else error.OutOfBounds;
            },
            else => return error.UnsupportedType,
        }
    }

    pub fn toInt(self: anytype, comptime T: type) com.ColorTypeError!Self{
        const oldtype = @TypeOf(self.cyan);
        const typeinfo = @typeInfo(oldtype);
        if (oldtype == T) return Self{
            .red = self.red,
            .green = self.green,
            .blue = self.blue,
            .alpha = self.alpha,
        };
        switch (typeinfo) {
            .Int => |info| {
                return switch (info.signedness) {
                    .unsigned => Self{
                        .cyan = try com.channelToInt(T, self.cyan),
                        .magenta = try com.channelToInt(T, self.magenta),
                        .yellow = try com.channelToInt(T, self.yellow),
                        .black = try com.channelToInt(T, self.black),
                    },
                    else => error.UnsupportedType,
                };
            },
            .Float => return Self{
                .cyan = try com.channelToInt(T, self.cyan),
                .magenta = try com.channelToInt(T, self.magenta),
                .yellow = try com.channelToInt(T, self.yellow),
                .black = try com.channelToInt(T, self.black),
            },
            else => return error.UnsupportedType,
        }
    }

    pub fn toFloat(self: anytype, comptime T: type) com.ColorTypeError!Self {
        const oldtype = @TypeOf(self.cyan);
        const typeinfo = @typeInfo(oldtype);
        switch (typeinfo) {
            .Int => |info| {
                return switch (info.signedness) {
                    .unsigned => Self{
                        .cyan = @intToFloat(T, self.cyan) / 100,
                        .magenta = @intToFloat(T, self.magenta) / 100,
                        .yellow = @intToFloat(T, self.yellow) / 100,
                        .black = @intToFloat(T, self.black) / 100,
                    },
                    else => error.UnsupportedType,
                };
            },
            .Float => return Self{
                .cyan = @floatCast(T, self.cyan),
                .magenta = @floatCast(T, self.magenta),
                .yellow = @floatCast(T, self.yellow),
                .black = @floatCast(T, self.black),
            },
            else => return error.UnsupportedType,
        }
    }

    pub fn toRGBA(self: anytype, comptime T: type) com.ColorTypeError!RGBA {
        switch(@typeInfo(T)) {
            .Int => |info| {
                const floated = try self.toFloat(f64);
                const inv_k = 1 - floated.black;
                const max = @intToFloat(f64, math.maxInt(T));
                return switch (info.signedness) {
                    // TODO - account for range being 0-100
                    .unsigned => RGBA{
                        .red = @floatToInt(T, math.round(max * (1 - floated.cyan) * inv_k)),
                        .green = @floatToInt(T, math.round(max * (1 - floated.magenta) * inv_k)),
                        .blue = @floatToInt(T, math.round(max * (1 - floated.yellow) * inv_k)),
                        .alpha = math.maxInt(T),
                    },
                    else => error.UnsupportedType,
                };
            },
            .Float => {
                const floated = try self.toFloat(T);
                const inv_k = 1 - floated.black;
                return RGBA{
                    .red = (1 - floated.cyan) * inv_k,
                    .green = (1 - floated.magenta) * inv_k,
                    .blue = (1 - floated.yellow) * inv_k,
                    .alpha = 1.0,
                };
            },
            else => return error.UnsupportedType,
        }
    }

    pub fn toHSL(self: anytype, comptime T: type) com.ColorTypeError!HSL {
        const intermed = try self.toRGBA(T);
        return try intermed.toHSL(T);
    }

    pub fn toHex(self: anytype, allocator: mem.Allocator) ![]u8 {
        const rgba = try self.toRGBA(u8);
        return try rgba.toHex(allocator);
    }

    pub fn toHexZ(self: anytype, allocator: mem.Allocator) ![]u8 {
        const rgba = try self.toRGBA(u8);
        return try rgba.toHexZ(allocator);
    }

    pub fn fromHex(comptime T: type, hexcolor: []const u8) !CMYK {
        const rgba = try RGBA.fromHex(T, hexcolor);
        return try rgba.toCMYK(T);
    }

    pub fn toString(self: anytype, allocator: mem.Allocator) ![]u8 {
        return try fmt.allocPrint(allocator, "CMYK({d}, {d}, {d}, {d})",
            .{self.cyan, self.magenta, self.yellow, self.black});
    }

    pub fn toStringZ(self: anytype, allocator: mem.Allocator) ![]u8 {
        return try fmt.allocPrintZ(allocator, "CMYK({d}, {d}, {d}, {d})",
            .{self.cyan, self.magenta, self.yellow, self.black});
    }
};

test "new: unsupportedType: Int" {
    _ = CMYK.new(i8, 0, 0, 0, 0) catch |err| switch (err) {
        error.UnsupportedType => return {},
        else => |e| return e,
    };
    try testing.expect(false);
}

test "new: unsupportedType: bool" {
    _ = CMYK.new(bool, true, false, true, false) catch |err| switch (err) {
        error.UnsupportedType => return {},
        else => |e| return e,
    };
    try testing.expect(false);
}

test "new: OutOfBounds: negative" {
    _ = CMYK.new(f32, -0.1, 0.0, 0.0, 0.0) catch |err| switch (err) {
        error.OutOfBounds => return {},
        else => |e| return e,
    };
    try testing.expect(false);
}

test "new: OutOfBounds: high" {
    _ = CMYK.new(f32, 0.1, 0.0, 2.0, 0.0) catch |err| switch (err) {
        error.OutOfBounds => return {},
        else => |e| return e,
    };
    try testing.expect(false);
}

test "toFloat: int" {
    const color = try CMYK.new(u8, 23, 14, 85, 15);
    const floated = try color.toFloat(f64);
    try testing.expect(floated.cyan == 0.23);
    try testing.expect(floated.magenta == 0.14);
    try testing.expect(floated.yellow == 0.85);
    try testing.expect(floated.black == 0.15);
}

test "toFloat: float" {
    const color = try CMYK.new(f32, 0.23, 0.14, 0.85, 0.15);
    const floated = try color.toFloat(f64);
    try testing.expect(math.round(floated.cyan * 10000) == 2300);
    try testing.expect(math.round(floated.magenta * 10000) == 1400);
}

test "toRGBA: int to int" {
    const color = try CMYK.new(u8, 23, 14, 85, 15);
    const rgbc = try color.toRGBA(u8);
    try testing.expect(rgbc.red == 167);
    try testing.expect(rgbc.green == 186);
    try testing.expect(rgbc.blue == 33);
    try testing.expect(rgbc.alpha == 255);
}

test "toHSL: int to int" {
    const color = try CMYK.new(u8, 37, 100, 15, 22);
    const hsl = try color.toHSL(u32);
    try testing.expect(hsl.hue == 284);
    try testing.expect(hsl.saturation == 100);
    try testing.expect(hsl.luminence == 33);
}

test "toHex" {
    const cmyk = try CMYK.new(u8, 100, 0, 100, 0);
    const hex = try cmyk.toHex(testing.allocator);
    defer testing.allocator.free(hex);
    try testing.expect(mem.eql(u8, hex, "#00ff00"));
}

test "toString" {
    const color = try CMYK.new(u8, 23, 0, 85, 15);
    const color_str = try color.toString(testing.allocator);
    defer testing.allocator.free(color_str);
    try testing.expect(mem.eql(u8, color_str, "CMYK(23, 0, 85, 15)"));
}

test {
    @import("std").testing.refAllDecls(@This());
}
