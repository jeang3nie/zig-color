const std = @import("std");
const math = std.math;

pub const ColorTypeError = error {
    UnsupportedType,
    OutOfBounds,
    IllegalCharacter,
    InvalidHexString,
};

pub fn checkBounds(float: anytype) bool {
    return if (float >= 0.0 and float <= 1.0) true else false;
}

pub fn channelToInt(comptime T:type, num: anytype) ColorTypeError!T {
    const oldtype = @TypeOf(num);
    const typeinfo = @typeInfo(oldtype);
    switch (typeinfo) {
        .Int => {
            const percent = @intToFloat(f64, num) / @intToFloat(f64, math.maxInt(oldtype));
            const new = percent * @intToFloat(f64, math.maxInt(T));
            return @floatToInt(T, math.round(new));
        },
        .Float => {
            return @floatToInt(T, math.round(num * @intToFloat(oldtype, math.maxInt(T))));
        },
        else => return .UnsupportedType,
    }
}

fn charToInt(c: u8) ColorTypeError!u8 {
    return switch (c) {
        '0' ... '9' => c - '0',
        'A' ... 'F' => c - 'A' + 10,
        'a' ... 'f' => c - 'a' + 10,
        else => ColorTypeError.IllegalCharacter,
    };
}

pub fn parseU8(buf: []const u8) !u8 {
    var x: u8 = 0;
    for (buf) |c| {
        const digit = try charToInt(c);
        if (digit >= 16) {
            return error.InvalidChar;
        }
        if (@mulWithOverflow(u8, x, 16, &x)) {
            return error.Overflow;
        }
        if (@addWithOverflow(u8, x, digit, &x)) {
            return error.Overflow;
        }
    }
    return x;
}
