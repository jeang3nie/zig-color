const std = @import("std");
const fmt = std.fmt;
const math = std.math;
const mem = std.mem;
const testing = std.testing;
const com = @import("common.zig");
const CMYK = @import("cmyk.zig").CMYK;
const HSL = @import("hsl.zig").HSL;
const RGBA = @import("rgba.zig").RGBA;

pub const RGB = struct {
    red: anytype,
    green: anytype,
    blue: anytype,

    const Self = @This();

    pub fn new(comptime T: type, red: T, green: T, blue: T) com.ColorTypeError!Self {
        switch (@typeInfo(T)) {
            .Int => |info| {
                return switch (info.signedness) {
                    .unsigned => Self{
                        .red = red,
                        .green = green,
                        .blue = blue,
                    },
                    // We don't support signed integers
                    else => error.UnsupportedType,
                };
            },
            .Float => {
                // First do some bounds checking
                return if (com.checkBounds(red)
                    and com.checkBounds(green)
                    and com.checkBounds(blue)) Self{
                    .red = red,
                    .green = green,
                    .blue = blue,
                } else error.OutOfBounds;
            },
            else => return error.UnsupportedType,
        }
    }

    pub fn toInt(self: anytype, comptime T: type) com.ColorTypeError!Self {
        const oldtype = @TypeOf(self.red);
        const typeinfo = @typeInfo(oldtype);
        // Types are identical, regurgitate everything as is
        if (oldtype == T) return Self{
            .red = self.red,
            .green = self.green,
            .blue = self.blue,
        };
        switch (typeinfo) {
            .Int =>  {
                return Self{
                    .red = try com.channelToInt(T, self.red),
                    .green = try com.channelToInt(T, self.green),
                    .blue = try com.channelToInt(T, self.blue),
                };
            },
            .Float => {
                return if (com.checkBounds(self.red)
                    and com.checkBounds(self.green)
                    and com.checkBounds(self.blue)) Self{
                    .red = try com.channelToInt(T, self.red),
                    .green = try com.channelToInt(T, self.green),
                    .blue = try com.channelToInt(T, self.blue),
                } else error.OutOfBounds;
            },
            else => return error.UnsupportedType,
        }
    }

    pub fn toFloat(self: anytype, comptime T: type) com.ColorTypeError!Self {
        const oldtype = @TypeOf(self.red);
        const typeinfo = @typeInfo(oldtype);
        if (oldtype == T) return Self{
            .red = self.red,
            .green = self.green,
            .blue = self.blue,
        };
        return switch (typeinfo) {
            .Int => Self{
                .red = @intToFloat(T, self.red) / @intToFloat(T, math.maxInt(oldtype)),
                .green = @intToFloat(T, self.green) / @intToFloat(T, math.maxInt(oldtype)),
                .blue = @intToFloat(T, self.blue) / @intToFloat(T, math.maxInt(oldtype)),
            },
            .Float => Self{
                .red = @floatCast(T, self.red),
                .green = @floatCast(T, self.green),
                .blue = @floatCast(T, self.blue),
            },
            else => error.UnsupportedType,
        };
    }

    pub fn toHSL(self: anytype, comptime T: type) com.ColorTypeError!HSL {
        const typeinfo = @typeInfo(T);
        const fl = try self.toFloat(f64);
        const max = math.max3(fl.red, fl.green, fl.blue);
        const min = math.min3(fl.red, fl.green, fl.blue);
        const range = max - min;
        const lum = (max + min) / 2.0;
        if (min == max) {
            return switch (typeinfo) {
                .Int => if (math.maxInt(T) < 360) error.UnsupportedType else
                    try HSL.new(T, 0, 0, @floatToInt(T, math.round(lum * 100))),
                .Float => try HSL.new(T, 0.0, 0.0, @floatCast(T, lum)),
                else => error.UnsupportedType,
            };
        }
        const saturation = if (lum == 0.0) 0.0 else range / (1.0 - math.absFloat(2.0 * lum - 1.0));
        const h = switch (max) {
            fl.red => (fl.green - fl.blue) / range,
            fl.green => 2.0 + (fl.blue - fl.red) / range,
            else => 4.0 + (fl.red - fl.green) / range,
        };
        const hu = h * 60;
        const hue = if (hu > 0) hu else hu + 360;
        return switch (typeinfo) {
            .Int => if (math.maxInt(T) < 360) error.UnsupportedType else
                try HSL.new(T,
                    @floatToInt(T, math.round(hue)),
                    @floatToInt(T, math.round(saturation * 100)),
                    @floatToInt(T, math.round(lum * 100))),
            .Float => try HSL.new(T, @floatCast(T, hue), @floatCast(T, saturation), @floatCast(T, lum)),
            else => error.UnsupportedType,
        };
    }

    pub fn toCMYK(self: anytype, comptime T: type) com.ColorTypeError!CMYK {
        const exp = try self.toFloat(f64);
        const largest = math.max3(exp.red, exp.green, exp.blue);
        const k = 1.0 - largest;
        const kinv = 1.0 - k;
        const c = (1.0 - exp.red - k) / kinv;
        const m = (1.0 - exp.green - k) / kinv;
        const y = (1.0 - exp.blue - k) / kinv;
        return switch (@typeInfo(T)) {
            .Int => try CMYK.new(T,
                @floatToInt(T, math.round(c * 100)),
                @floatToInt(T, math.round(m * 100)),
                @floatToInt(T, math.round(y * 100)),
                @floatToInt(T, math.round(k * 100))),
            .Float => try CMYK.new(T, @floatCast(T, c), @floatCast(T, m), @floatCast(T, y), @floatCast(T, k)),
            else => error.UnsupportedType,
        };
    }

    pub fn toHex(self: anytype, allocator: mem.Allocator) ![]u8 {
        const conv = try self.toInt(u8);
        return try fmt.allocPrint(allocator, "#{x:0>2}{x:0>2}{x:0>2}", .{ conv.red, conv.green, conv.blue });
    }

    pub fn toHexZ(self: anytype, allocator: mem.Allocator) ![]u8 {
        const conv = try self.toInt(u8);
        return try fmt.allocPrintZ(allocator, "#{x:0>2}{x:0>2}{x:0>2}", .{ conv.red, conv.green, conv.blue });
    }

    pub fn fromHex(comptime T: type, hexcolor: []const u8) !RGB {
        if (hexcolor[0] != '#') return .InvalidHexString;
        if (mem.len(hexcolor) != 7) return .InvalidHexString;
        const red = try com.parseU8(hexcolor[1..3]);
        const green = try com.parseU8(hexcolor[3..5]);
        const blue = try com.parseU8(hexcolor[5..7]);
        const color = try Self.new(u8, red, green, blue);
        switch (type) {
            u8 => return color,
            else => {
                const typeinfo = @typeInfo(T);
                return switch (typeinfo) {
                    .Int => try color.toInt(T),
                    .Float => try color.toFloat(T),
                    else => error.UnsupportedType,
                };
            },
        }
    }

    pub fn toString(self: anytype, allocator: mem.Allocator) ![]u8 {
        return try fmt.allocPrint(allocator, "RGB({d}, {d}, {d})",
            .{self.red, self.green, self.blue});
    }

    pub fn toStringZ(self: anytype, allocator: mem.Allocator) ![]u8 {
        return try fmt.allocPrintZ(allocator, "RGB({d}, {d}, {d})",
            .{self.red, self.green, self.blue});
    }
};

test "new: u8" {
    const red = try RGB.new(u8, 255, 0, 0);
    try testing.expect(red.red == 255);
    try testing.expect(red.green == 0);
    try testing.expect(red.blue == 0);
}

test "new: u16" {
    const color = try RGB.new(u16, 32639, 0, 51400);
    _ = color;
}

test "new: f32" {
    const red = try RGB.new(f32, 0.5, 0.0, 1.0);
    try testing.expect(red.red == 0.5);
    try testing.expect(red.green == 0.0);
    try testing.expect(red.blue == 1.0);
}

test "new: f64" {
    const color = try RGB.new(f64, 0.5, 0.0, 1.0);
    try testing.expect(color.red == 0.5);
    try testing.expect(color.green == 0.0);
    try testing.expect(color.blue == 1.0);
}

test "new: SignedIntError" {
    _ = RGB.new(i8, 0, 0, 0) catch |err| switch (err) {
        error.UnsupportedType => return {},
        else => |e| return e,
    };
    try testing.expect(false);
}

test "new: OutOfBounds: negative" {
    _ = RGB.new(f32, -0.1, 0.0, 0.0) catch |err| switch (err) {
        error.OutOfBounds => return {},
        else => |e| return e,
    };
    try testing.expect(false);
}

test "new: OutOfBounds: high" {
    _ = RGB.new(f32, 0.1, 0.0, 2.0) catch |err| switch (err) {
        error.OutOfBounds => return {},
        else => |e| return e,
    };
    try testing.expect(false);
}

test "toInt: int" {
    const color = try RGB.new(u8, 127, 0, 200);
    const new = try color.toInt(u16);
    try testing.expect(new.red == 32639);
    try testing.expect(new.green == 0);
    try testing.expect(new.blue == 51400);
}

test "toInt: float" {
    const color = try RGB.new(f64, 0.5, 0.5, 0.5);
    const new = try color.toInt(u8);
    try testing.expect(new.red == new.green);
    try testing.expect(new.blue == new.red);
    try testing.expect(new.red == 128);
}

test "toInt: equal" {
    const color = try RGB.new(u8, 255, 255, 255);
    const new = try color.toInt(u8);
    try testing.expect(color.red == new.red);
}

test "toFloat: int" {
    const color = try RGB.new(u8, 127, 0, 200);
    const new = try color.toFloat(f64);
    try testing.expect(new.red == 0.4980392156862745);
    try testing.expect(new.green == 0);
    try testing.expect(new.blue == 0.7843137254901961);
}

test "toFloat: float" {
    const color = try RGB.new(f32, 0.9, 0.3, 0.1);
    const new = try color.toFloat(f64);
    try testing.expect(new.red == 0.8999999761581421);
    try testing.expect(new.green == 0.30000001192092896);
    try testing.expect(new.blue == 0.10000000149011612);
}

test "toCMYK: int -> float" {
    const color = try RGB.new(u8, 127, 0, 200);
    const new = try color.toCMYK(f64);
    try testing.expect(new.cyan == 0.365);
    try testing.expect(new.magenta == 1);
    try testing.expect(new.yellow == 0);
    try testing.expect(new.black == 0.21568627450980393);
}

test "toCMYK: int -> int" {
    const color = try RGB.new(u8, 127, 0, 200);
    const new = try color.toCMYK(u8);
    try testing.expect(new.cyan == 37);
    try testing.expect(new.magenta == 100);
    try testing.expect(new.yellow == 0);
    try testing.expect(new.black == 22);
}

test "toHSL: int -> int" {
    const color = try RGB.new(u8, 127, 34, 210);
    const hsl = try color.toHSL(u16);
    try testing.expect(hsl.hue == 272);
    try testing.expect(hsl.saturation == 72);
    try testing.expect(hsl.luminence == 48);
}

test "toHSL: int -> float" {
    const color = try RGB.new(u8, 34, 27, 150);
    const hsl = try color.toHSL(f64);
    try testing.expect(hsl.hue == 243.41463414634146);
    try testing.expect(hsl.saturation == 0.6949152542372882);
    try testing.expect(hsl.luminence == 0.34705882352941175);
}

test "toHSL: u8" {
    const color = try RGB.new(u8, 0, 0, 0);
    _ = color.toHSL(u8) catch |err| switch (err) {
        error.UnsupportedType => return {},
        else => |e| return e,
    };
    try testing.expect(false);
}

test "toHex: int" {
    const color = try RGB.new(u8, 127, 255, 0);
    const hex = try color.toHex(testing.allocator);
    defer testing.allocator.free(hex);
    try testing.expect(mem.eql(u8, hex, "#7fff00"));
}

test "fromHex: u8" {
    const buf: []const u8 = "#fe4b3c";
    const color = try RGB.fromHex(u8, buf);
    try testing.expect(color.red == 254);
    try testing.expect(color.green == 75);
    try testing.expect(color.blue == 60);
}

test "fromHex: f64" {
    const buf: []const u8 = "#fe4b3c";
    const color = try RGB.fromHex(f64, buf);
    try testing.expect(color.red == 0.996078431372549);
    try testing.expect(color.green == 0.29411764705882354);
    try testing.expect(color.blue == 0.23529411764705882);
}

test "toString" {
    const blue = try RGB.new(u8, 0, 0, 255);
    const blue_str = try blue.toString(testing.allocator);
    defer testing.allocator.free(blue_str);
    try testing.expect(mem.eql(u8, blue_str, "RGB(0, 0, 255)"));
}

test {
    @import("std").testing.refAllDecls(@This());
}
