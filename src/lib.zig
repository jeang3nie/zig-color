usingnamespace @import("cmyk.zig");
usingnamespace @import("hsl.zig");
usingnamespace @import("rgba.zig");
usingnamespace @import("rgb.zig");
pub const ColorTypeError = @import("common.zig").ColorTypeError;
